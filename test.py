import numpy as np
import pandas as pd


def _entropy(ids, target):
    # calculate entropy of a node with index ids
    if len(ids) == 0: return 0
    ids = [i + 1 for i in ids]  # panda series index starts from 1
    freq = np.array(target[ids].value_counts())
    print(target[ids])
    return freq


if __name__ == "__main__":
    df = pd.DataFrame.from_csv('weather.csv')
    X = df.iloc[:, :-1]
    y = df.iloc[:, -1]
    Ntrain = X.count()[0]
    ids = range(Ntrain)
    fr = _entropy(ids, y)
    print(fr)
