class TreeNode(object):
    def __init__(self, ids=None, children=[], entropy=0, depth=0):
        self.ids = ids  # index of data in this node
        self.entropy = entropy  # entropy, will fill later
        self.depth = depth  # distance to root node
        self.split_attribute = None  # which attribute is chosen, if non-leaf
        self.children = children  # list of its child nodes
        self.order = None  # order of values of split_attribute in children
        self.label = None  # label of node if it is a leaf

    def set_properties(self, split_attribute, order):
        self.split_attribute = split_attribute
        self.order = order

    def set_label(self, label):
        self.label = label
