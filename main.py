import pandas as pd
from decision_tree_id3 import DecisionTreeID3

if __name__ == "__main__":
    df = pd.DataFrame.from_csv('weather.csv')
    X = df.iloc[:, :-1]
    y = df.iloc[:, -1]
    tree = DecisionTreeID3(max_depth=3, min_samples_split=2)
    tree.fit(X, y)
    print(tree.predict(X))
